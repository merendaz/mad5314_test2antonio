#include "InternetButton.h"

/* This InternetButton library has some useful functions.
Here we blink ALL the LEDs instead of just one.*/

InternetButton b = InternetButton();
int DELAY = 4000;
void setup() {
// Use b.begin(1); if you have the original SparkButton, which does not have a buzzer or a plastic enclosure
// to use, just add a '1' between the parentheses in the code below.
// Tell b to get everything ready to go
b.begin();
for (int i=0; i<3; i++) {
b.allLedsOn(255,0,0);
delay(2000);
b.allLedsOff();
}


// Exposed functions-test

// 1.
Particle.function("coordWithPhone", coordWP);
}

void loop(){

}

int coordWP(String cmd) {
if (cmd == "start") {
delay(DELAY);
b.allLedsOn(0,20,20);

//1
delay(DELAY);
b.ledOff (2);
b.ledOff(10);
//2
delay(DELAY);
b.ledOff (3);
b.ledOff(9);
//3
delay(DELAY);
b.ledOff (4);
b.ledOff(8);
//4
delay(DELAY);
b.ledOff (5);
b.ledOff(7);
//5
delay(DELAY);
b.ledOff (6);
//6
delay(DELAY);
b.ledOff (1);
b.ledOff (11);
b.ledOn(2, 255, 0, 0);
b.ledOn(10, 255, 0, 0);
b.ledOn(4, 255, 0, 0);
b.ledOn(5, 255, 0, 0);
b.ledOn(7, 255, 0, 0);
b.ledOn(8, 255, 0, 0);
delay(DELAY);
}
else if (cmd == "stop") {
b.allLedsOff();
}
else {
return -1;
}
// function succesfully finished
return 1;
}


