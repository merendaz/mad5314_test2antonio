//
//  ViewController.swift
//  MAD5314_Test2_Antonio
//
//  Created by Antonio Merendaz do Carmo Nt on 2019-11-07.
//  Copyright © 2019 Medtouch. All rights reserved.
//

import UIKit
import Particle_SDK

class ViewController: UIViewController {
    // MARK: User variables
    let USERNAME = "merendaz@gmail.com"
    let PASSWORD = "Nantme2019;"
    
    // MARK: Device
    let DEVICE_ID = "390031000f47363333343437"
    var myPhoton : ParticleDevice?
    
    // MARK: Other variables
    var questionNumber = 0
    var gameScore: Int = 0
    var particleEvent: String = "playerChoice"
    var eventListenerID : Any?
    var handler : Any?
    
    var timeElapsed     = 0
    var slowDownTime    = 0
    var time: Double    = 0.0
    
    @IBOutlet weak var timeElapsedLbL: UILabel!
    @IBOutlet weak var amountsMohammedSlider: UISlider!
    @IBOutlet weak var timeSlowsDownByLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // 1. Initialize the SDK
        ParticleCloud.init()
//        print("NUM QUESTIONS: \(self.questions.count)")
        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")
                // try to get the device
                self.getDeviceFromCloud()
            }
        } // end login
    }

    // MARK: Get Device from Cloud
    // Gets the device from the Particle Cloud
    // and sets the global device variable
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon from cloud: \(device?.id)")
                self.myPhoton = device
                
                // subscribe to events
//                self.subscribeToParticleEvents(event: self.particleEvent)
            }
            
        } // end getDevice()
    }
    
    func startParticle() {
        
        print("StartParticle")
        
        let parameters = ["start"]
        var task = myPhoton!.callFunction("coordWithPhone", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to start")
            }
            else {
                print("Error when telling Particle to start")
            }
        }
    }
    
    func slowDownParticle() {
        
        print("Slow Down Particle")
        let slowDown = "\(self.slowDownTime)"
        print("SLOWDOWN: \(slowDown)")
        let parameters = [slowDown]
        var task = myPhoton!.callFunction("slowDown", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to slowDown")
            }
            else {
                print("Error when telling Particle to slowDown")
            }
        }
    }
    
    
    
    @IBAction func startMonitoringBtnPressed(_ sender: UIButton) {
//        self.timeElapsed
        self.time = Double(self.timeElapsed) + Double(self.slowDownTime)
        print("TIME: \(self.time)")
        self.startParticle()
       
        let timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            if (self.timeElapsed < (20 + self.slowDownTime*2)) {
//                self.startParticle()
                
//                DispatchQueue.main.async {
                self.timeElapsed += 1
                self.timeElapsedLbL.text = "\(self.timeElapsed)"
                
//                }
            }
            else {
                timer.invalidate()
                self.timeElapsed = 0
            }
        }
        
    }
    
    
    @IBAction func amountsMohamedSliderChange(_ sender: UISlider) {
        
    
        self.slowDownTime = Int(self.amountsMohammedSlider.value*10)
        self.timeSlowsDownByLbl.text = "\(self.slowDownTime)"
        self.time = Double(self.timeElapsed) + Double(self.slowDownTime)
         self.slowDownParticle()
    }
    
}

